CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * FAQ
 * Maintainers

 INTRODUCTION
 ------------
 The File Details module provides a field formatter for media file entity types.
 This module provides the following functionality:
 - Display media name instead of file name
 - Display mime icon before link text
 - Display mime icon after link text
 - Open link in new tab
 - Show mime type information
 - Show extension type information
 - Show file size information

 Additionally to providing the ability to display additional file information,
 content editors have the ability to control the order in which these items are displayed.
 Users can control the order through the following fields:
 - Mime type display order
 - Extension type display order
 - Show file size information

 Users can control the order of the display by entering a value between 1 - 3 for each field.
 These values will then dictate the order in which the additional information fields are displayed.

  * For a full description of the module, visit the project page:
    https://www.drupal.org/project/file_details

  * To submit bug reports and feature suggestions, or track changes:
    https://www.drupal.org/project/issues/file_details

REQUIREMENTS
------------
The File Details module only requires the Drupal core Field module,
no third-party contributed modules or libraries are required.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-----------------------------------
The File Details field formatter works with file media types and requires that
the Drupal core Field UI module be enabled. Field UI provides a "Manage display"
page for the media entity type. From this page you can select the "File Details"
formatter to use for each corresponding field. From the entity page click on
the formatter's gear icon to the right of the field row. This presents the formatter's
configuration page. Fill in the fields accordingly and click "Update" to save that configuration,
and "Save" to save the display.

FAQ
---
Q: Styling seems to be minimal

A: Styling has been kept minimal, so implementation can be personalised to meet any needs.
The markup structure has been created to be versatile. Users can quite easily create a 2 col
layout using the "show mime icon after text" option.

MAINTAINERS
-----------
Current maintainers:
 * Joseph Henriquez - https://www.drupal.org/user/3653904/

This project has been sponsored by:
 * Doghouse Agency - specializing in developing drupal solutions . Visit https://doghouse.agency/ for more information
