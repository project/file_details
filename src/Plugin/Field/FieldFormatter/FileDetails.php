<?php

namespace Drupal\file_details\Plugin\Field\FieldFormatter;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'file_details' formatter.
 *
 * @FieldFormatter(
 *   id = "file_details",
 *   label = @Translation("File Details"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class FileDetails extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelFactoryInterface $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      // Additional services.
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManager $entity_type_manager, LoggerChannelFactoryInterface $loggerFactory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->entityTypeManager = $entity_type_manager;
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'general' => [
          'show_mime_icon_before' => 0,
          'show_mime_icon_after' => 0,
          'show_new_tab' => 0,
          'wrapper_class' => '',
        ],
        'media' => [
          'use_media_name' => 0,
          'override_media_name' => '',
        ],
        'mime' => [
          'show' => 0,
          'order' => NULL,
        ],
        'extension' => [
          'show' => 0,
          'order' => NULL,
        ],
        'size' => [
          'show' => 0,
          'order' => NULL,
        ],
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General Configurations'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    // Mime grouping key.
    $general_settings = $this->getSetting('general');

    $form['general']['wrapper_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Wrapper Class'),
      '#description' => $this->t('Adds a class at the top level of the element. Defaults to: file-details.'),
      '#default_value' => $general_settings['wrapper_class'] ?? '',
    ];

    $form['general']['show_mime_icon_before'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show mime icon before text'),
      '#description' => $this->t('Indicates whether the mime icon should be displayed before the text or not'),
      '#default_value' => $general_settings['show_mime_icon_before'] ?? 0,
    ];

    $form['general']['show_mime_icon_after'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show mime icon after text'),
      '#description' => $this->t('Indicates whether the mime icon should be displayed after the text or not'),
      '#default_value' => $general_settings['show_mime_icon_after'] ?? 0,
    ];

    $form['general']['show_new_tab'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open link in new tab'),
      '#description' => $this->t('Indicates whether the link should be opened in a new tab or not'),
      '#default_value' => $general_settings['show_new_tab'] ?? 0,
    ];

    $form['media'] = [
      '#type' => 'details',
      '#title' => $this->t('Media Configurations'),
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    // Override grouping key.
    $media_settings = $this->getSetting('media');

    $form['media']['use_media_name'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use media name'),
      '#description' => $this->t('Indicates whether the media name should be displayed instead of the file name'),
      '#default_value' => $media_settings['use_media_name'] ?? 0,
    ];

    $form['media']['override_media_name'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Override media name with custom text'),
      '#description' => $this->t('Media name with custom input text'),
      '#default_value' => $media_settings['override_media_name'] ?? 0,
      '#attributes' => [
        'id' => 'override_media_name',
      ],
    ];

    $form['media']['fixed_media_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fixed text for media'),
      '#description' => $this->t('Fixed text for media file name'),
      '#default_value' => $media_settings['fixed_media_text'] ?? NULL,
      // Show and require only if ['extension']['show'] is checked.
      '#states' => [
        'enabled' => [
          ':input[id="override_media_name"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[id="override_media_name"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['mime'] = [
      '#type' => 'details',
      '#title' => $this->t('File mime Configurations'),
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    // Mime grouping key.
    $mime_settings = $this->getSetting('mime');

    $form['mime']['show'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show mime type information'),
      '#description' => $this->t('Indicates whether the mime type should be visible or not.'),
      '#attributes' => [
        'id' => 'mime_type_checkbox',
      ],
      '#default_value' => $mime_settings['show'] ?? 0,
    ];

    $form['mime']['order'] = [
      '#type' => 'number',
      '#title' => $this->t('Mime type display order'),
      '#description' => $this->t('Order dictates display order of mime type'),
      '#default_value' => $mime_settings['order'] ?? NULL,
      // Max determined by maximum amount of items displayed.
      '#max' => 3,
      // Show and require only if ['mime']['show'] is checked.
      '#states' => [
        'enabled' => [
          ':input[id="mime_type_checkbox"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[id="mime_type_checkbox"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['extension'] = [
      '#type' => 'details',
      '#title' => $this->t('File extension Configurations'),
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    // Extension grouping key.
    $extension_settings = $this->getSetting('extension');

    $form['extension']['show'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show extension type information'),
      '#description' => $this->t('Indicates whether the extension type should be visible or not.'),
      '#default_value' => $extension_settings['show'] ?? 0,
      '#attributes' => [
        'id' => 'extension_type_checkbox',
      ],
    ];

    $form['extension']['order'] = [
      '#type' => 'number',
      '#title' => $this->t('Extension type display order'),
      '#description' => $this->t('Order dictates display order of extension type'),
      '#default_value' => $extension_settings['order'] ?? NULL,
      // Max determined by maximum amount of items displayed.
      '#max' => 3,
      // Show and require only if ['extension']['show'] is checked.
      '#states' => [
        'enabled' => [
          ':input[id="extension_type_checkbox"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[id="extension_type_checkbox"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['size'] = [
      '#type' => 'details',
      '#title' => $this->t('File size Configurations'),
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    // Size grouping key.
    $size_settings = $this->getSetting('size');

    $form['size']['show'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show file size information'),
      '#description' => $this->t('Indicates whether the file size should be visible or not.'),
      '#default_value' => $size_settings['show'] ?? 0,
      '#attributes' => [
        'id' => 'file_size_checkbox',
      ],
    ];

    $form['size']['order'] = [
      '#type' => 'number',
      '#title' => $this->t('File size display order'),
      '#description' => $this->t('Order dictates display order of file size'),
      '#default_value' => $size_settings['order'] ?? NULL,
      // Max determined by maximum amount of items displayed.
      '#max' => 3,
      // Show and require only if ['size']['show'] is checked.
      '#states' => [
        'enabled' => [
          ':input[id="file_size_checkbox"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[id="file_size_checkbox"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    // General grouping key.
    $general_settings = $this->getSetting('general');

    // Mime grouping key.
    $media_settings = $this->getSetting('media');

    // Mime grouping key.
    $mime_settings = $this->getSetting('mime');

    // Extension grouping key.
    $extension_settings = $this->getSetting('extension');

    // Size grouping key.
    $size_settings = $this->getSetting('size');

    // General summary details.
    $summary[] = (!empty($general_settings['wrapper_class'])) ? 'Class: ' . $general_settings['wrapper_class'] : 'Class: Default';
    $summary[] = (!empty($general_settings['override_media_name'])) ? $this->t('Override media name : Yes') : $this->t('Override media name : No');
    $summary[] = (!empty($general_settings['show_mime_icon_before'])) ? $this->t('Display mime type icon before text : Yes') : $this->t('Display mime type icon before text : No');
    $summary[] = (!empty($general_settings['show_mime_icon_after'])) ? $this->t('Display mime type icon after text : Yes') : $this->t('Display mime type icon after text : No');
    $summary[] = (!empty($general_settings['show_new_tab'])) ? $this->t('Open link in new tab : Yes') : $this->t('Open link in new tab : No');

    // Media summary details.
    $summary[] = (!empty($media_settings['use_media_name'])) ? $this->t('Display media name : Yes') : $this->t('Display media name : No');
    $summary[] = (!empty($media_settings['override_media_name'])) ? $this->t('Override media name : Yes') : $this->t('Override media name : No');
    $summary[] = 'Media Override Text: ' . $media_settings['fixed_media_text'];

    // Mime summary details.
    $summary[] = (!empty($mime_settings['show'])) ? $this->t('Display mime type : Yes') : $this->t('Display mime type : No');
    $summary[] = 'Mime Type display order: ' . $mime_settings['order'];

    // Extension summary details.
    $summary[] = (!empty($extension_settings['show'])) ? $this->t('Display extension type : Yes') : $this->t('Display extension type : No');
    $summary[] = 'Extension type order: ' . $extension_settings['order'];

    // File size summary details.
    $summary[] = (!empty($size_settings['show'])) ? $this->t('Display file size : Yes') : $this->t('Display file size : No');
    $summary[] = 'File size order: ' . $size_settings['order'];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    try {
      $elements = [];

      // Grouping keys.
      $general_settings = $this->getSetting('general');
      $media_settings = $this->getSetting('media');

      // Check if referenced entity exists.
      if (empty($items->referencedEntities()[0])) {
        // Return no value.
        return [];
      }

      /** @var \Drupal\file\Entity\File $referenced_file */
      $referenced_file = $items->referencedEntities()[0];

      // Retrieve file id from referenced entity.
      $fid = $referenced_file->id();

      /** @var \Drupal\file\FileStorage $file_storage */
      $file_storage = $this->entityTypeManager->getStorage('file');

      /** @var \Drupal\file\Entity\File $file */
      $file = $file_storage->load($fid);

      // Returns information about a file path.
      $file_path_info = pathinfo($file->getFilename());

      // Returns file name.
      $file_name = $this->cleanString($file_path_info['filename']);

      // Return file url.
      /** @var \Drupal\Core\Url $file_url */
      $file_url = \Drupal::service('file_url_generator')->generate($file->getFileUri());

      // Return file extension.
      $file_extension = $file_path_info['extension'] ?? '';

      // Return file size formatted.
      $file_size = format_size($file->getSize());

      // Return mime type.
      $mime_type = $file->getMimeType();

      // Initialise empty arrays.
      $mime_icon_classes_before = [];
      $blank_target = [];

      // Evaluate if show mime icon before option is selected.
      if (!empty($general_settings['show_mime_icon_before'])) {
        // Classes to add to options.
        $mime_icon_classes_before = [
          'attributes' => [
            'class' => [
              'file',
              // Add a specific class for each and every mime type.
              'file--mime-' . strtr($mime_type, ['/' => '-', '.' => '-']),
              // Add a more general class for groups of well known MIME types.
              'file--' . file_icon_class($mime_type),
            ],
            'type' => $mime_type,
          ],
        ];
      }

      // Initialise file class after string.
      $mime_icon_classes_after = '';

      // Evaluate if show mime icon after option is selected.
      if (!empty($general_settings['show_mime_icon_after'])) {
        $mime_icon_classes_after = 'file ' .
          'file-after ' .
          // Add a specific class for each and every mime type.
          'file--mime-' . strtr($mime_type, ['/' => '-', '.' => '-']) . ' ' .
          // Add a more general class for groups of well known MIME types.
          'file--' . file_icon_class($mime_type);
      }

      // Evaluate if show new tab option is selected.
      if (!empty($general_settings['show_new_tab'])) {
        $blank_target = [
          // Set blank target attribute.
          'attributes' => [
            'target' => '_blank',
          ],
        ];
      }

      // Modifier icon class. Can be used as a selector to add custom icons.
      $mime_modifier_class = file_icon_class($mime_type);

      // Merge both link options together.
      $link_options = array_merge_recursive($mime_icon_classes_before, $blank_target);

      // Set selected attributes.
      $file_url->setOptions($link_options);

      // Evaluate if use media name option is selected.
      if (!empty($media_settings['use_media_name'])) {
        /** @var \Drupal\media\Entity\Media $media */
        $media = $items->getEntity();

        if ($media != NULL) {
          $file_name = $media->getName();
        }
      }

      if ($media_settings['override_media_name'] && !empty($media_settings['fixed_media_text'])) {
        $file_name = $media_settings['fixed_media_text'];
      }

      // Construct link.
      $file_link = Link::fromTextAndUrl($file_name, $file_url)->toString();

      // Return form values.
      $display_options = $this->getSettings();

      // Ignore general settings and return options only.
      unset($display_options['general']);

      // Initialise selected values array.
      $selected_values = [];

      foreach ($display_options as $type => $display_option) {
        // Evaluate if option is set not and break from current iteration.
        if (isset($display_option['show']) && $display_option['show'] === 0) {
          continue;
        }

        // Evaluate $type i.e. mime and assign value accordingly.
        switch ($type) {
          case "mime":
            $value = $mime_type;
            break;

          case "extension":
            $value = $file_extension;
            break;

          case "size":
            $value = $file_size;
            break;
        }

        if (isset($display_option['order'])) {
          // Push order number along with type and value to selected values array.
          $selected_values[$display_option['order']] = [
            'type' => $type,
            'value' => $value,
          ];
        }
      }

      // Sort array by order index.
      ksort($selected_values);

      // Put everything in an array for theming.
      $elements[] = [
        '#theme' => 'file_details',
        // Attach drupal's library of file icons.
        '#attached'   => [
          'library'   => [
            'file/drupal.file',
            'file_details/file-details',
          ],
        ],
        '#wrapper_class' => (!empty($general_settings['wrapper_class'])) ? $general_settings['wrapper_class'] : 'file-details',
        '#file_link' => $file_link,
        '#file_details' => $selected_values,
        '#is_mime_icon_before' => $general_settings['show_mime_icon_before'] ?? 0,
        '#is_mime_icon_after' => $general_settings['show_mime_icon_after'] ?? 0,
        '#mime_icon_class_after' => $mime_icon_classes_after,
        '#mime_modifier_class' => $mime_modifier_class,
        '#is_new_window' => $general_settings['show_new_tab'] ?? 0,
      ];

      return $elements;
    }
    catch (\Exception $e) {
      // Retrieve file id from referenced entity.
      $fid = $referenced_file->id();

      // Return error message if file id is not returned.
      $this->loggerFactory->get($fid)->error($e);
      // Return no value.
      return [];
    }

  }

  /**
   * Removes unwanted characters from string.
   *
   * @param string $string
   *   The string to clean.
   *
   * @return string
   *   Returns string stripped of characters.
   */
  public function cleanString(string $string) {
    // Characters to remove.
    $strip = ["~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
      "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
      "â€”", "â€“", ",", "<", ".", ">", "/", "?",
    ];

    // Search string and remove unwanted characters with space.
    $clean = trim(str_replace($strip, " ", strip_tags($string)));
    return preg_replace('/\s+/', " ", $clean);
  }

}
